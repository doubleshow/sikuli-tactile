/*******************************************************************************
 * Copyright 2011 sikuli.org
 * Released under the MIT license.
 * 
 * Contributors:
 *     Tom Yeh - initial API and implementation
 ******************************************************************************/
package org.sikuli.makerbot;

import static com.googlecode.javacv.cpp.opencv_core.*;
import static com.googlecode.javacv.cpp.opencv_imgproc.*;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import org.sikuli.core.logging.ImageExplainer;
import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroupFile;

import com.google.common.collect.Lists;
import com.googlecode.javacpp.Loader;
import com.googlecode.javacv.cpp.opencv_core.*;

public class PolygonSetPrintableGenerator extends DefaultPrintableGenerator{

	static private ImageExplainer logger = new ImageExplainer("poly");
	static STGroupFile g = new STGroupFile("templates/scad.stg", "utf-8", '$', '$');

	protected void processPolygon(CvSeq polygon_points, IplImage image, int level){
		CvPoint cvPoints = new CvPoint(polygon_points.total());
		cvCvtSeqToArray(polygon_points, cvPoints, CV_WHOLE_SEQ);
		cvDrawContours(image, polygon_points, CvScalar.RED, CvScalar.RED, -1, 5, CV_AA);


		IplImage polygonMask = IplImage.create(cvGetSize(image), 8, 1);
		cvSet(polygonMask,CvScalar.BLACK,null);
		//cvDrawContours(polygonMask, polygon_points, CvScalar.WHITE, CvScalar.WHITE, -1, 5, CV_AA);
		cvDrawContours(polygonMask, polygon_points, CvScalar.WHITE, CvScalar.WHITE, -1, 1, CV_AA);

		//logger.log("polygonMask", polygonMask.getBufferedImage());

		//if (level < 2)
		//computeExtrusiblePolygon(polygonMask,level);
		//computeExtrusiblePolygon(polygon_points, level);
	}


	private void printContour(CvSeq polygon_points){
		int total = polygon_points.total();		
		for (int i = 0; i < total; i++) {	
			//CvPoint points.
			CvPoint p = new CvPoint(cvGetSeqElem(polygon_points, i));
			System.out.print("[" + p.x() + "," + p.y() + "]");
			if (i<total-1){
				System.out.print(",");
			}
		}
	}


	//	class Polygon {
	//		
	//		List<Point> points;
	//		
	//		List<Point> getPoints(){
	//			return points;
	//		}
	//	}


	List<Point> cvPointSeq2PointList(CvSeq cvPointSeq){
		List<Point> points = new ArrayList<Point>();
		int total = cvPointSeq.total();
		for (int i = 0; i < total; i++) {	
			CvPoint p = new CvPoint(cvGetSeqElem(cvPointSeq, i));
			points.add(new Point(p.x(),p.y()));
		}
		return points;
	}

	List<Integer> generateIndices(int n){
		List<Integer> indices = new ArrayList<Integer>();
		for (int i = 0; i < n; i++) {	
			indices.add(i);
		}
		return indices;
	}	


	//private void computeExtrusiblePolygon(IplImage polygonMask, int level){

	private void computeExtrusiblePolygon(CvSeq polygon_points, int level){
	

//		CvMemStorage storage = CvMemStorage.create();		
//		int contour_method = CV_RETR_TREE;		
//		CvSeq contour = new CvSeq(null);
//		cvFindContours(polygonMask, storage, contour, Loader.sizeof(CvContour.class),
//				contour_method, CV_CHAIN_APPROX_SIMPLE);
//
//
//
//
//
//
//
//		//		add("pageName",pageName).
//		//		add("items",sections);
//		//		
//		//		System.out.println("\nlinear_extrude(height = 20, center = true)");
//
//		CvSeq polygon_points = cvApproxPoly(contour, Loader.sizeof(CvContour.class),
//				storage, CV_POLY_APPROX_DP, cvContourPerimeter(contour)*0.005, 0);

		//int outer_n = polygon_points.total();



		List<Point> ps = cvPointSeq2PointList(polygon_points);
		List<Integer> indices = generateIndices(polygon_points.total());


		if (ps.size() > 5){

			ST polygonST = g.getInstanceOf("linear_extrude_polygon");		
			polygonST.add("points", ps);
			polygonST.add("indices", indices);
			polygonST.add("offset", level*10 + 5);
			System.out.println(polygonST.render());
		}
		//			ST pointST 
		//			System.out.print("[" + p.x() + "," + p.y() + "]");
		//			if (i<total-1){
		//				System.out.print(",");
		//			}
	}


	//		System.out.print("polygon(");
	//		
	//		System.out.print("points = [");
	//		printContour(polygon_points);

	//		CvSeq inner = contour.v_next();

	//		if (inner != null){
	//			polygon_points = cvApproxPoly(inner, Loader.sizeof(CvContour.class),
	//					storage, CV_POLY_APPROX_DP, cvContourPerimeter(contour)*0.005, 0);
	//		}else{
	//			
	//			
	//			
	//			
	//			//List<ST> sectionItems = Lists.newArrayList();
	//			
	//			
	////			System.out.println("],");
	////			System.out.print("\tpaths = [[");		
	////			for (int i = 0; i < outer_n-1; i++) {	
	////				System.out.print(""+i+",");
	////			}
	////			System.out.print(""+(outer_n-1));
	////			System.out.println("]]);");
	//			return;
	//		}
	//		
	////		System.out.println(",");
	//		
	//		int inner_n = polygon_points.total();		

	//		System.out.print("\t\t");
	//		printContour(polygon_points);
	//		System.out.println("],");
	//
	//		
	//		System.out.print("\tpaths = [[");		
	//		for (int i = 0; i < outer_n-1; i++) {	
	//			System.out.print(""+i+",");
	//		}
	//		System.out.print(""+(outer_n-1));
	//		
	//		System.out.println("],");
	//		
	//		System.out.print("\t\t[");		
	//		for (int i = 0; i < inner_n-1; i++) {	
	//			System.out.print(""+(outer_n+i)+",");
	//		}
	//		System.out.print(""+(outer_n+inner_n-1)+"]");		
	//		System.out.print("]");
	//		
	//		System.out.println(");");


}

