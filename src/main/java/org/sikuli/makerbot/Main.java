package org.sikuli.makerbot;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;

import javax.imageio.ImageIO;

import org.sikuli.core.logging.ImageExplainer;
import org.sikuli.makerbot.solid.LotusRootSetSolid;
import org.sikuli.makerbot.solid.Solid;
import org.sikuli.makerbot.solid.SolidFactory;

import com.google.common.base.Charsets;
import com.google.common.io.Files;
import com.sampullara.cli.Args;
import com.sampullara.cli.Argument;

public class Main {
	
	
	static class Command {
		 @Argument(value = "input", description = "This is the input file (e.g., figure.png)", required = true)
	        private String inputFilename;

		 @Argument(value = "output", description = "This is the output file (e.g., figure.scad)", required = false)
	        private String outputFilename;
		 
		 @Argument(value = "debug", description = "This flag can optionally be set to write explanation images to log", required = false)
	        private boolean debugFlag;
		 
	}

	public static void main(String[] args) {
		Command c = new Command();
        Args.usage(c);
        Args.parse(c, args);
        
        if (c.debugFlag){
        	ImageExplainer.getExplainer(LotusRootSetSolid.class).setLevel(ImageExplainer.Level.STEP);
        }
        		
		try {			
			BufferedImage input = ImageIO.read(new File(c.inputFilename));
			Solid solid = SolidFactory.createLotusRootSetSolidModel(input);
			
			if (c.outputFilename != null)			
				Files.write(solid.toSCAD(), new File(c.outputFilename), Charsets.UTF_8 );
			else
				System.out.println(solid.toSCAD());
			
		} catch (IOException e) {
			e.printStackTrace();
		}		
		

	}

}
