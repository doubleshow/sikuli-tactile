/*******************************************************************************
 * Copyright 2011 sikuli.org
 * Released under the MIT license.
 * 
 * Contributors:
 *     Tom Yeh - initial API and implementation
 ******************************************************************************/
package org.sikuli.makerbot;

import static com.googlecode.javacv.cpp.opencv_core.CV_AA;
import static com.googlecode.javacv.cpp.opencv_core.CV_WHOLE_SEQ;
import static com.googlecode.javacv.cpp.opencv_core.cvCvtSeqToArray;
import static com.googlecode.javacv.cpp.opencv_core.cvDrawContours;
import static com.googlecode.javacv.cpp.opencv_core.cvFillPoly;
import static com.googlecode.javacv.cpp.opencv_core.cvGetSeqElem;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;

import org.stringtemplate.v4.ST;
import org.stringtemplate.v4.STGroupFile;

import com.googlecode.javacv.cpp.opencv_core.CvPoint;
import com.googlecode.javacv.cpp.opencv_core.CvScalar;
import com.googlecode.javacv.cpp.opencv_core.CvSeq;
import com.googlecode.javacv.cpp.opencv_core.IplImage;

public class ElevationMapPrintableGenerator extends DefaultPrintableGenerator{

	protected void processPolygon(CvSeq polygon_points, IplImage image, int level){

		CvPoint cvPoints = new CvPoint(polygon_points.total());
		cvCvtSeqToArray(polygon_points, cvPoints, CV_WHOLE_SEQ);

		CvScalar level_color = getColorForLevel(level);

		int[] npts = {polygon_points.total()};
		cvFillPoly(image, cvPoints, npts, 1, level_color, CV_AA, 0);
		cvDrawContours(image, polygon_points, CvScalar.BLUE, CvScalar.BLUE, -1, 1, CV_AA);
		
		computeExtrusiblePolygon(polygon_points, level);
	}
	
	static STGroupFile g = new STGroupFile("templates/scad.stg", "utf-8", '$', '$');
	
	List<Point> cvPointSeq2PointList(CvSeq cvPointSeq){
		List<Point> points = new ArrayList<Point>();
		int total = cvPointSeq.total();
		for (int i = 0; i < total; i++) {	
			CvPoint p = new CvPoint(cvGetSeqElem(cvPointSeq, i));
			points.add(new Point(p.x(),p.y()));
		}
		return points;
	}

	List<Integer> generateIndices(int n){
		List<Integer> indices = new ArrayList<Integer>();
		for (int i = 0; i < n; i++) {	
			indices.add(i);
		}
		return indices;
	}	


	private void computeExtrusiblePolygon(CvSeq polygon_points, int level){
	

		List<Point> ps = cvPointSeq2PointList(polygon_points);
		List<Integer> indices = generateIndices(polygon_points.total());


		//if (level )
		
		if (ps.size() > 5){

			ST polygonST = g.getInstanceOf("linear_extrude_polygon");		
			polygonST.add("points", ps);
			polygonST.add("indices", indices);
			
			if (level % 2 == 0){
				polygonST.add("offset", level*10 + 5);				
			}else{
				polygonST.add("offset", level*10 + 5);		
			}
			
			//polygonST.add("offset", level*10 + 5);
			
			//System.out.println(polygonST.render());
		}
	}





}
