/*******************************************************************************
 * Copyright 2011 sikuli.org
 * Released under the MIT license.
 * 
 * Contributors:
 *     Tom Yeh - initial API and implementation
 ******************************************************************************/
package org.sikuli.makerbot.solid;

import static com.googlecode.javacv.cpp.opencv_core.*;
import static com.googlecode.javacv.cpp.opencv_imgproc.*;
import com.googlecode.javacpp.Loader;
import com.googlecode.javacv.cpp.opencv_core.*;

import java.awt.Point;
import java.util.ArrayList;
import java.util.List;


public class Contour {	
	private CvSeq cvSeq = null;
	static public Contour createFrom(CvSeq cvSeq){
		Contour p = new Contour();
		p.cvSeq = cvSeq;
		return p;
	}
	public CvSeq getCvSeq() {
		return cvSeq;
	}

	public List<Point> toPoints(){
		CvMemStorage storage = CvMemStorage.create();
//		CvSeq polygon_points = cvApproxPoly(cvSeq, Loader.sizeof(CvContour.class),
//				storage, CV_POLY_APPROX_DP, cvContourPerimeter(cvSeq)*0.002, 0);
//		return cvPointSeq2PointList(polygon_points);
		return cvPointSeq2PointList(cvSeq);
	}

	List<Point> cvPointSeq2PointList(CvSeq cvPointSeq){
		List<Point> points = new ArrayList<Point>();
		int total = cvPointSeq.total();
		for (int i = 0; i < total; i++) {	
			CvPoint p = new CvPoint(cvGetSeqElem(cvPointSeq, i));
			points.add(new Point(p.x(),p.y()));
		}
		return points;
	}

}
